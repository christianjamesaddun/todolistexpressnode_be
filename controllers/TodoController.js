const toDoListData = [
  {
    todo_id: 1,
    todolist_title: "Task 1",
    isCompleted: false,
  },
];

exports.getTodoData = (req, res) => {
  return res.status(201).json(toDoListData);
};

exports.addTodoData = (req, res) => {
  const { todolist_title } = req.body;
  if (!todolist_title) {
    return res.status(400).json({ error: "Title is required" });
  }
  const newTodo = {
    todo_id: toDoListData.length + 1,
    todolist_title,
    isCompleted: false,
  };
  toDoListData.push(newTodo);
  return res.status(201).json(toDoListData);
};

exports.updateTodoData = (req, res) => {
  const { id } = req.params;

  // Find the index of the todo item in the list
  const todoIndex = toDoListData.findIndex(
    (todo) => todo.todo_id === parseInt(id)
  );

  if (todoIndex === -1) {
    return res.status(404).json({ error: "Todo not found" });
  }

  // Update the todo item
  toDoListData[todoIndex].isCompleted = !toDoListData[todoIndex].isCompleted;

  return res.status(200).json(toDoListData[todoIndex]);
};
