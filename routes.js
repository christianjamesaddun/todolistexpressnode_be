module.exports = function (app) {
  //ROUTES
  const todolist_routes = require('./routes/todolist.routes')

  app.use('/todo_list', todolist_routes)
}