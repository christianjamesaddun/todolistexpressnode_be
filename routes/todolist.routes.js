const express = require("express");
const toDoListController = require("../controllers/TodoController");
const router = express.Router();

router.get("/get_data", toDoListController.getTodoData);
router.post("/add_data", toDoListController.addTodoData);
router.put("/update_data/:id", toDoListController.updateTodoData);

module.exports = router;
