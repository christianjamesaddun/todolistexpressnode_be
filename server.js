const express = require("express");
const app = express();
const server = require("http").createServer(app);
const cors = require("cors");
const PORT = 3000;

const allowedOrigins = ["http://localhost:5173", "http://localhost:3000"];

app.use(
  cors({
    origin: function (origin, callback) {
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        const msg =
          "The CORS policy for this site does not " +
          "allow access from the specified Origin.";
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
  })
);

app.use(express.json());

app.get("/", (_req, res) => {
  res.status(200).json({ message: "Routes are alive" });
});

require("./routes")(app);
server.listen(PORT, () => {
  console.log(`Server running on http://localhost:${PORT}`);
  console.log(
    `For get data api use METHOD GET and url: http://localhost:${PORT}/todo_list/get_data`
  );
  console.log(
    `For add data api use METHOD POST and url: http://localhost:${PORT}/todo_list/add_data`
  );
  console.log(
    `For update data api use METHOD PUT and url: http://localhost:${PORT}/todo_list/update_data/:id`
  );
});
